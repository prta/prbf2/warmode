# Warmode Script v1.1 29.12.2018
# Coders: Rage, cassius23
# This script is a rework of original script by Rage
# https://www.realitymod.com/forum/showthread.php?p=2036474

from collections import OrderedDict

import host

import game.realityadmin as radmin
import game.realityconfig_admin as rconfigadmin
import game.realitytimer as rtimer

import warmode_config


def init():
    return Warmode()


class Warmode():
    _instance = None

    def __new__(cls):
        if cls._instance:
            cls._instance = super(Warmode, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        self.configure()

        if warmode_config.INITIALIZE:
            self.init()

    def configure(self):
        self.tags = self.prepareTags(warmode_config.TAGS_TEAM1,
                                     warmode_config.TAGS_TEAM2)

        self.AUTO_ASSIGN = warmode_config.AUTO_ASSIGN
        self.FLIP_TEAMS = warmode_config.FLIP_TEAMS
        self.KICK_ON_SWITCH = warmode_config.KICK_ON_SWITCH
        self.SPECIAL_TAGS = warmode_config.SPECIAL_TAGS

        self.COMMAND_POWER_LEVEL = warmode_config.COMMAND_POWER_LEVEL

    def init(self):
        # Disable autobalance
        rconfigadmin.smb_enabled = False

        self.registerHandlers()
        self.registerCommand()

    @classmethod
    def prepareTags(cls, team1, team2):
        tags = {tag: 1 for tag in team1}
        tags.update({tag: 2 for tag in team2})
        return tags

    def registerHandlers(self):
        host.registerHandler('PlayerConnect', self.onPlayerConnect)
        host.registerHandler('PlayerChangeTeams', self.onPlayerChangeTeams)

    def registerCommand(self):
        radmin.addCommand('warmode', self.commandWarmode, self.COMMAND_POWER_LEVEL)

    def isSpecialPlayer(self, p):
        name = p.getName()
        return bool([tag for tag in self.SPECIAL_TAGS if tag in name])

    def onPlayerConnect(self, p):
        if self.isSpecialPlayer(p):
            return

        if self.AUTO_ASSIGN:
            radmin.playerInit(p)

            # Delay team assignment - player might be in a state
            # in which it's not possible to switch him.
            rtimer.fireNextTick(self.assignToTeam, p)

    def onPlayerChangeTeams(self, p, humanHasSpawned):
        if self.isSpecialPlayer(p):
            return

        if self.KICK_ON_SWITCH:
            self.kickPlayer(p, 'Kick on switch')

        if self.AUTO_ASSIGN:
            self.assignToTeam(p)

    def assignToTeam(self, p):
        name = p.getName()

        team = next(
            (team for tag, team in self.tags.iteritems() if tag in name),
            None # default
        )

        if team is None:
            self.kickPlayer(p, 'Missing or invalid tag')

        if self.FLIP_TEAMS:
            team = 1 if team == 2 else 2

        p.setTeam(team)

    @staticmethod
    def kickPlayer(p, reason):
        host.rcon_invoke('admin.kickPlayer %d' % p.index)
        radmin.adminPM( "%s has been kicked, %s" % (p.getName(), reason))

    ############
    # Commands #
    ############
    def commandWarmode(self, args, p):
        if len(args) == 0:
            args.append('status')

        command = args.pop(0)

        try:
            self.commands.get(command)(self, args, p)
        except (ValueError, IndexError):
            self.commands.get('help')(self, args, p)

    def commandStatus(self, args, p):
        '''!warmode: print warmode status'''

        msgs = [
            'Warmode status:',
            'autoassign: %s' % self.AUTO_ASSIGN,
            'flipteams: %s' % self.FLIP_TEAMS,
            'kickonswitch: %s' % self.KICK_ON_SWITCH,
        ]
        for msg in msgs:
            radmin.personalMessage(msg, p)

    def commandHelp(self, args, p):
        '''!warmode help: print command usage'''

        radmin.personalMessage('Warmode command usage:', p)
        for command in self.commands.values():
            radmin.personalMessage(command.__doc__, p)

    def commandAutoAssign(self, args, p):
        '''!warmode autoassign on/off: enable/disable autoassign'''
        new_mode = args[0]

        self.AUTO_ASSIGN = self.switchMode(new_mode)
        radmin.globalMessage('autoassign is %s' % new_mode)

    def commandFlipTeams(self, args, p):
        '''!warmode flipteams on/off: enable/disable flipteams'''
        new_mode = args[0]

        old_flip = self.FLIP_TEAMS
        self.FLIP_TEAMS = self.switchMode(new_mode)

        # Swapteams if flipteams mode changes
        # (it performs the swap only during briefing)
        if old_flip != self.FLIP_TEAMS:
            radmin.commandSwapTeam([], p) # empty array == no additional arguments

        radmin.globalMessage('flipteams is %s' % new_mode)

    def commandKickOnSwitch(self, args, p):
        '''!warmode kickonswitch on/off: enable/disable kick on switch'''
        new_mode = args[0]

        self.KICK_ON_SWITCH = self.switchMode(new_mode)
        radmin.globalMessage('kickonswitch is %s' % new_mode)

    commands = OrderedDict([
        ('help', commandHelp),
        ('status', commandStatus),
        ('autoassign', commandAutoAssign),
        ('flipteams', commandFlipTeams),
        ('kickonswitch', commandKickOnSwitch)
    ])

    @staticmethod
    def switchMode(to):
        if to == 'on':
            return True
        elif to == 'off':
            return False
        raise ValueError
